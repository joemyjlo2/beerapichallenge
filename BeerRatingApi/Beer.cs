﻿
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace BeerRatingApi
{
    public class Beer
    {
        public int ID { get; set; }
        public string Name { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public List<UserRating> UserRatings { get; set; } = new List<UserRating>();
    }

    public interface IUserRatingService
    {
        dynamic InsertRating(int id, UserRating rating);
    }

    public class UserRatingService : IUserRatingService
    {
        public dynamic InsertRating(int id, UserRating rating) => DBmanager.AddRatingToBeer(id, rating);
    }


    public class UserRating
    {
        //The regex for the username to be used by the minimal validator to determine correctness and return the appropriate message
        [RegularExpression(@"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$",
        ErrorMessage = "The username must be a valid email address such as contoso13@mymail.com")]
        public string Username { get; set; }

        [Range(1, 5, ErrorMessage = "The rating index must be between 1 and 5.")]
        public int Rating { get; set; }

        // Reject comments that have more than 1000 characters.
        [StringLength(1000, ErrorMessage = "The comments must not exceed 1000 characters.")]
        public string Comments { get; set; } = string.Empty;
    }
}