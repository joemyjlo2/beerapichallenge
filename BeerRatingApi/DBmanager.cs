﻿using Newtonsoft.Json;
using RestSharp;
using System.Net;

namespace BeerRatingApi
{
    public static class DBmanager
    {
        static string getBeerURL = "";
        static string getMultipleBeersURL = "";
        static int beersPerPage = 0;
        static List<Beer> beerList;


        public static Beer GetBeer(int id)
        {
            Beer? beer = beerList.FirstOrDefault(b => b.ID == id);

            //Search for the beer on Punk API only if our database does not contain it
            if (beer == null)
            {
                //Update the external endpoint url in case it's empty
                if (string.IsNullOrEmpty(getBeerURL))
                    GetConfigParameters();

                if (UrlIsValid(getBeerURL))
                {
                    RestRequest rest = new();
                    RestResponse response = new RestClient($"{getBeerURL}/{id}").Execute(rest);

                    if (response != null && response.StatusCode == HttpStatusCode.OK)
                        beer = JsonConvert.DeserializeObject<List<Beer>>(response.Content)?.FirstOrDefault();
                    if (beer != null)
                        InsertBeer(beer);
                }
            }
            return beer;
        }


        public static List<Beer> RetrieveBeerObjects()
        {
            if (string.IsNullOrEmpty(getMultipleBeersURL))
                GetConfigParameters();

            // Punk API returns 25 records per request, and up to 80 if a number is specified.
            // In this project, that number is specified in the config file
            // So the page number at for Punk Api depends on how many records already inserted in our DB
            int page = (beerList.Count / beersPerPage) + 1;

            if (UrlIsValid(getMultipleBeersURL))
            {
                RestRequest rest = new();
                RestResponse response = new RestClient($"{getMultipleBeersURL}page={page}&per_page={beersPerPage}").Execute(rest);

                if (response != null && response.StatusCode == HttpStatusCode.OK)
                {
                    var beers = JsonConvert.DeserializeObject<List<Beer>>(response.Content);
                    // At this point, if all the beers are successfully retrieved, we have a list of beers without user ratings.
                    // So the already saved beers should not be overwritten because their user ratings would be lost
                    // So the current database should be looped over and appended the objects that it does not contain already 
                    // Eventually another method can be created to synchronize the current database with ApiPunk periodically, maybe up to 3 to 4 times a day 

                    foreach (var beer in beers)
                    {
                        if (beerList.FirstOrDefault(x => x.ID == beer.ID) == null)
                            beerList.Add(beer);
                    }
                    beerList = beerList.OrderBy(x => x.ID).ToList();
                    SaveJsonDB();
                }
                GC.Collect();//Assure not leaving unnecessary objects in memory
            }
            //int index = Math.Min(beerList.Count, (page - 1) * beersPerPage);
            //int count = Math.Min(beerList.Count - index - 1, beersPerPage);
            //return beerList.GetRange(index, count);
            return beerList;
        }


        public static dynamic AddRatingToBeer(int id, UserRating rating)
        {
            //If we get here, rating object has been sucessfully validated 

            Beer? beer = beerList.FirstOrDefault(b => b.ID == id);
            //If the beer is not yet in our database, look it up on punkAPI
            if (beer == null)
                beer = GetBeer(id);

            if (beer != null)
            {
                // If the specified user already rated the specified bear, update the rating but don't duplicate it
                var existingRating = beer.UserRatings.FirstOrDefault(r => r.Username == rating.Username);
                if (existingRating != null)
                    foreach (var p in rating.GetType().GetProperties())
                    {
                        var val = p.GetValue(rating);
                        if (val != null)//Copy the value of each non null property 
                            existingRating.GetType().GetProperty(p.Name).SetValue(existingRating, val);
                    }
                else
                    beer.UserRatings.Add(rating);
                SaveJsonDB();
                return "Rating added successfully.";
            }

            // The API endpoint requires any error to come within a dictionary object 
            return new Dictionary<string, string[]>
            {{ "id", new string[] { "The beer you tried to rate does not exist." } }};
        }



        static void InsertBeer(Beer beer)
        {
            if (beer != null)
            {
                if (beerList == null)
                    beerList = new List<Beer>();

                //Only insert the new beer in the local db if it does not exist therein
                if (beerList.FirstOrDefault(b => b.ID == beer.ID) == null)
                {
                    beerList.Add(beer);
                    SaveJsonDB();
                }
            }
        }

        //Load the local database into memory for objects manipulation
        public static void InitiateBeerDB()
        {
            try
            {
                string path = Directory.GetCurrentDirectory() + "\\Database\\database.json";
                string content = File.ReadAllText(path);
                beerList = JsonConvert.DeserializeObject<List<Beer>>(content);
                if (beerList == null)
                    beerList = new List<Beer>();
            }
            catch (Exception)
            {
                beerList = new();
            }
        }

        // The parameters below are stored in a config file to avoid hardcoding them
        // which would require recompilation when any of them is changed
        private static void GetConfigParameters()
        {
            try
            {
                string path = Directory.GetCurrentDirectory() + "\\Config.json";
                string content = File.ReadAllText(path);
                var obj = JsonConvert.DeserializeObject<dynamic>(content);
                getBeerURL = obj.getBeerURL;
                getMultipleBeersURL = obj.getMultipleBeersURL;
                beersPerPage = obj.beersPerPage;
            }
            catch (Exception)
            {
                getBeerURL = "";
                getMultipleBeersURL = "";
                beersPerPage = 1;
            }

        }

        static bool UrlIsValid(string url)
        {
            return Uri.IsWellFormedUriString(url, UriKind.Absolute);
        }

        static void SaveJsonDB()
        {
            try
            {
                string path = Directory.GetCurrentDirectory() + "\\Database\\database.json";
                File.WriteAllText(path, JsonConvert.SerializeObject(beerList));
            }
            catch (Exception) { }
        }
    }
}
