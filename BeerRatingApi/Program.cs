using BeerRatingApi;
using System.Text.RegularExpressions;
using MiniValidation;
// This MiniValidation class referenced above is the equivalent of the Model validator in Asp.Net MVC in this minimal API
// The validator takes the validation attributes on the model to determine correct values


var builder = WebApplication.CreateBuilder(args);

builder.Services.AddScoped<IUserRatingService, UserRatingService>();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

//Load the local database into memory on start up
DBmanager.InitiateBeerDB();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();



app.MapGet("/api/getbeerlist", () =>
{
    var currentBeerList = DBmanager.RetrieveBeerObjects();

    if (currentBeerList != null)
        return Results.Ok(currentBeerList);

    var errorResp = new Dictionary<string, string[]>
    {{ "Errors", new[] { "The remote API could not be contacted." } }};

    return Results.ValidationProblem(errorResp);

}).WithName("SingleBeer");



/// The parameter id is meant to be an integer. However, if the user submits a very large integer, the binding will throw an exception
/// So to manage this exception internally, it is necessary to receive the id as a string and properly handle the conversion
/// in case of incoming wrong data
app.MapPost("/api/addrating", (string id, UserRating rating, IUserRatingService ratingService) =>
{
    // Assure the string id contains only numbers and no single or leading 0;
    if (!new Regex("^[1-9][0-9]{0,8}$").IsMatch(id))
    {
        var error = new Dictionary<string, string[]>
            {{ "id", new string[] { "ID must be a non-zero positive integer no greater than 999999999." } }};
        return Results.ValidationProblem(error);
    }

    int beerID = Convert.ToInt32(id);

    if (MiniValidator.TryValidate(rating, out var errors))
        return Results.Ok(ratingService.InsertRating(beerID, rating));

    return Results.ValidationProblem(errors);

}).WithName("AddBeerRating");


app.Run();


public partial class Program { }