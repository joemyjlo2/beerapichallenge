using System.Net;
using System.Net.Http.Json;
using BeerRatingApi;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Testing;


namespace BeerRatingApiTest
{
    public class FetchBeersTest
    {
        [Fact]
        public async Task FetchAllBeers()
        {
            await using var application = new WebApplicationFactory<Program>()
                .WithWebHostBuilder(builder => builder
                    .ConfigureServices(services => { }));

            var client = application.CreateClient();

            var result = await client.GetAsync("/api/getbeerlist");

            Assert.Equal(HttpStatusCode.OK, result.StatusCode);

            var beers = await result.Content.ReadFromJsonAsync<List<Beer>>();
            Assert.NotNull(beers);
            Assert.True(beers?.Count > 10, "The list of beer must have more than 10 elements");

        }
    }
}