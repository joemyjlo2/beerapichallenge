using System.Net;
using System.Net.Http.Json;
using BeerRatingApi;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace BeerRatingApiTest
{

    public class TestUserRatingService : IUserRatingService
    {
        public dynamic InsertRating(int id, UserRating rating) => DBmanager.AddRatingToBeer(id, rating);
    }

    class TestingApplication : WebApplicationFactory<UserRating>
    {
        protected override IHost CreateHost(IHostBuilder builder)
        {
            builder.ConfigureServices(services =>
            {
                services.AddScoped<IUserRatingService, TestUserRatingService>();
            });

            return base.CreateHost(builder);
        }
    }


    public class UserRatingTest
    {
        [Fact]
        public async Task InsertRating()
        { // Add a valid rating to a valid beer and assess that the isertion is successful

            await using var application = new WebApplicationFactory<Program>()
                .WithWebHostBuilder(builder => builder
                    .ConfigureServices(services =>
                    {
                        services.AddScoped<IUserRatingService, TestUserRatingService>();
                    }));


            var client = application.CreateClient();

            var result = await client.PostAsJsonAsync("/api/addrating?id=8", new UserRating
            {
                Username = "ventritest@mail.com",
                Rating = 4,
                Comments = "Vintri has enabled us to establish a verified single source of truth for our supply chain."
            });

            Assert.Equal(HttpStatusCode.OK, result.StatusCode);
            Assert.Equal("\"Rating added successfully.\"", await result.Content.ReadAsStringAsync());
        }



        [Fact]
        public async Task InsertInvalidIdRatingID()
        {
            // Add a valid rating to an invalid beer id and assess that the isertion is fails and the related message is returned
            await using var application = new WebApplicationFactory<Program>()
                .WithWebHostBuilder(builder => builder
                    .ConfigureServices(services => { services.AddScoped<IUserRatingService, TestUserRatingService>(); }));

            var client = application.CreateClient();

            var result = await client.PostAsJsonAsync("/api/addrating?id=5885554545454", new UserRating()
            {
                Username = "ventritest@mail.com",
                Rating = 4,
                Comments = "Vintri has enabled us to establish a verified single source of truth for our supply chain."
            });

            Assert.Equal(HttpStatusCode.BadRequest, result.StatusCode);

            var validationResult = await result.Content.ReadFromJsonAsync<HttpValidationProblemDetails>();
            Assert.NotNull(validationResult);
            Assert.Contains("positive integer no greater than 999999999", validationResult!.Errors["id"][0]);
        }



        [Fact]
        public async Task InsertInvalidRatingBody()
        {
            await using var application = new WebApplicationFactory<Program>()
                .WithWebHostBuilder(builder => builder
                    .ConfigureServices(services => { services.AddScoped<IUserRatingService, TestUserRatingService>(); }));

            var client = application.CreateClient();


            string comments = "This is a simple test comment";
            // Duplicate the comments enough to exceed 1000 characters 
            for (int i = 0; i < 10; i++)
                comments += comments;

            var result = await client.PostAsJsonAsync("/api/addrating?id=5", new UserRating() { Username = "Ventri", Comments = comments });

            Assert.Equal(HttpStatusCode.BadRequest, result.StatusCode);

            var validationResult = await result.Content.ReadFromJsonAsync<HttpValidationProblemDetails>();
            Assert.NotNull(validationResult);
            Assert.Contains("such as contoso13@mymail.com", validationResult!.Errors["Username"][0]);
            Assert.Contains("between 1 and 5.", validationResult!.Errors["Rating"][0]);
            Assert.Contains("The comments must not exceed 1000 characters.", validationResult!.Errors["Comments"][0]);
        }
    }
}