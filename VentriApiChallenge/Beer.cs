﻿
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace VentriApiChallenge
{
    public class Beer
    {
        public int ID { get; set; }
        public string Name { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public List<UserRating> UserRatings { get; set; } = new List<UserRating>();
    }
}