﻿
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace VentriApiChallenge
{
    public class Config
    {
        public int BeersPerPage { get; set; }
        public string GetMultipleBeersURL { get; set; } = string.Empty;
        public string GetBeerURL { get; set; } = string.Empty;

        public static Config GetConfigParameters()
        {
            Config config = null;
            try
            {
                string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Config.json");
                string content = File.ReadAllText(path);
                config = JsonConvert.DeserializeObject<Config>(content);
            }
            catch (Exception e)
            { ErrorLogger.RegisterError("Config.GetConfigParameters", $"{e.Message}: {e.StackTrace}"); }

            return config;
        }
    }
}