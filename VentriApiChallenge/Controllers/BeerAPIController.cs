using Microsoft.AspNetCore.Mvc;
using System.Text.RegularExpressions;

namespace VentriApiChallenge.Controllers
{
    [Route("api/v1/[action]")]
    public class BeerAPIController : ControllerBase
    {
        private readonly ILogger<BeerAPIController> _logger;

        public BeerAPIController(ILogger<BeerAPIController> logger)
        {
            _logger = logger;
        }

        [HttpGet(Name = "getbeerlist")]
        public IResult GetBeerList() 
        {
            return Results.Ok(DBmanager.RetrieveBeerObjects());
        }

        // Doing this small project in a full .NET webApi offers the ability to use the action filter
        // In the minal project, the action filter looks legit but it never fires because the framework
        // Does not implement it internally. So here, the custom model validator is called based on the attribute
        // On the model's properties. The action filter is called before the action is executed

        [HttpPost(Name = "/api/v1/addrating")]
        [ValidateJsonBody]
        public IResult AddRating(string id, [FromBody] UserRating userRating)
        {
            // Assure the string id contains only numbers and no single or leading 0;
            if (!new Regex("^[1-9][0-9]{0,8}$").IsMatch(id))
                return Results.BadRequest("ID must be a non-zero positive integer no greater than 999999999."); 

            int beerID = Convert.ToInt32(id);
            return DBmanager.AddRatingToBeer(beerID, userRating);
        }
    }
}