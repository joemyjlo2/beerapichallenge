﻿using Newtonsoft.Json;
using RestSharp;
using System.Net;

namespace VentriApiChallenge
{
    public static class DBmanager
    {
        static readonly Config config = Config.GetConfigParameters();
        static List<Beer> beerList;


        public static Beer GetBeer(int id)
        {
            Beer? beer = beerList.FirstOrDefault(b => b.ID == id);

            //Search for the beer on Punk API only if our database does not contain it already
            if (beer == null)
            {
                if (UrlIsValid(config.GetBeerURL))
                {
                    try
                    {
                        RestRequest rest = new();
                        RestResponse response = new RestClient($"{config.GetBeerURL}/{id}").Execute(rest);

                        if (response?.Content != null && response.StatusCode == HttpStatusCode.OK)
                            beer = JsonConvert.DeserializeObject<List<Beer>>(response.Content)?.FirstOrDefault();
                        if (beer != null)
                            InsertBeer(beer);
                    }
                    catch (Exception e)
                    {
                        ErrorLogger.RegisterError("DBmanager.GetBeer", $"{e.Message}: {e.StackTrace}");
                    }
                }
            }
            ErrorLogger.RegisterError("DBmanager.GetBeer", "Beer not found");
            return beer;
        }


        public static List<Beer> RetrieveBeerObjects()
        {
            // Punk API returns 25 records per request, and up to 80 if a number is specified.
            // In this project, that number is specified in the config file
            // So the page number at for Punk Api depends on how many records already inserted in our DB

            if (beerList == null)
                beerList = new();
            int page = (beerList.Count / config.BeersPerPage) + 1;

            if (UrlIsValid(config.GetMultipleBeersURL))
            {
                try
                {
                    RestRequest rest = new();
                    RestResponse response = new RestClient($"{config.GetMultipleBeersURL}page={page}&per_page={config.BeersPerPage}").Execute(rest);

                    if (response?.Content != null && response.StatusCode == HttpStatusCode.OK)
                    {
                        var beers = JsonConvert.DeserializeObject<List<Beer>>(response.Content);
                        // At this point, if all the beers are successfully retrieved, we have a list of beers without user ratings.
                        // So the already saved beers should not be overwritten because their user ratings would be lost
                        // So the current database should be looped over and appended the objects that it does not contain already 
                        // Eventually another method can be created to synchronize the current database with ApiPunk periodically, maybe up to 3 to 4 times a day 
                        if (beers != null)
                            foreach (var beer in beers)
                            {
                                if (beerList.FirstOrDefault(x => x.ID == beer.ID) == null)
                                    beerList.Add(beer);
                            }

                        beerList = beerList.OrderBy(x => x.ID).ToList();
                        SaveJsonDB();
                    }
                    GC.Collect();//Assure not leaving unnecessary objects in memory
                }
                catch (Exception e)
                {
                    ErrorLogger.RegisterError("DBmanager.GetBeer", $"{e.Message}: {e.StackTrace}");
                }
            }
            return beerList;
        }


        public static IResult AddRatingToBeer(int id, UserRating rating)
        {
            //If we get here, rating object has been sucessfully validated 
            Beer? beer = GetBeer(id);

            if (beer != null)
            {
                // If the specified user already rated the specified bear, update the rating but don't duplicate it
                var existingRating = beer.UserRatings.FirstOrDefault(r => r.Username == rating.Username);
                if (existingRating != null)
                    foreach (var p in rating.GetType().GetProperties())
                    {
                        var val = p.GetValue(rating);
                        if (val != null)//Copy the value of each non null property 
                            existingRating.GetType().GetProperty(p.Name).SetValue(existingRating, val);
                    }
                else
                    beer.UserRatings.Add(rating);
                SaveJsonDB();
                return Results.Ok("Rating added successfully.");
            } 
            return Results.NotFound("Invalid beer ID.");
        }



        static void InsertBeer(Beer beer)
        {
            if (beer != null)
            {
                if (beerList == null)
                    beerList = new List<Beer>();

                //Only insert the new beer in the local db if it does not exist therein
                if (beerList.FirstOrDefault(b => b.ID == beer.ID) == null)
                {
                    beerList.Add(beer);
                    SaveJsonDB();
                }
            }
        }

        //Load the local database into memory for objects manipulation
        public static void InitiateBeerDB()
        {
            try
            {
                string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Database\database.json");
                string content = File.ReadAllText(path);
                beerList = JsonConvert.DeserializeObject<List<Beer>>(content);
                if (beerList == null)
                    beerList = new List<Beer>();
            }
            catch (Exception e)
            {
                ErrorLogger.RegisterError("DBmanager.InitiateBeerDB", $"{e.Message}: {e.StackTrace}");
                beerList = new();
            }
        }

        // The parameters below are stored in a config file to avoid hardcoding them
        // which would require recompilation when any of them is changed


        static bool UrlIsValid(string url)
        {
            return Uri.IsWellFormedUriString(url, UriKind.Absolute);
        }

        static void SaveJsonDB()
        {
            try
            {
                string path = Directory.GetCurrentDirectory() + "\\Database\\database.json";
                File.WriteAllText(path, JsonConvert.SerializeObject(beerList));
            }
            catch (Exception e)
            {
                ErrorLogger.RegisterError("DBmanager.SaveJsonDB", $"{e.Message}: {e.StackTrace}");
            }
        }
    }
}
