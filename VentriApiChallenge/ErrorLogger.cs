﻿
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace VentriApiChallenge
{
    public static class ErrorLogger
    {
        public static void RegisterError(string className, string message)
        {
            try
            {
                string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Error.log");
                string error = $"{DateTime.Now} - {className} -> {message}\n";
                File.AppendAllText(path, error);
            }
            catch (Exception) { }
        }
    }
}