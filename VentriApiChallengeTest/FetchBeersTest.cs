using System.Net;
using System.Net.Http.Json;
using VentriApiChallenge;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Testing;


namespace VentriApiChallengeTest
{
    public class FetchBeersTest
    {
        public Beer[] Value { get; set; }

        [Fact]
        public async Task FetchAllBeers()
        {
            await using var application = new WebApplicationFactory<Program>()
                .WithWebHostBuilder(builder => builder
                    .ConfigureServices(services => { }));



            var client = application.CreateClient();

            var result = await client.GetAsync("/api/v1/getbeerlist");

            Assert.Equal(HttpStatusCode.OK, result.StatusCode);

            var beers = await result.Content.ReadFromJsonAsync<FetchBeersTest>();
            Assert.NotNull(beers);
            Assert.NotNull(beers?.Value);

        }
    }
}