using System.Net.Http.Json;
using VentriApiChallenge;
using Microsoft.AspNetCore.Mvc.Testing;
using System.Net;
using Microsoft.AspNetCore.Http;
using Xunit.Abstractions;

namespace VentriApiChallengeTest
{

    public class UserRatingTest
    {

        private readonly ITestOutputHelper output;

        public UserRatingTest(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Fact]
        public async Task InsertRating()
        { // Add a valid rating to a valid beer and assess that the isertion is successful

            await using var application = new WebApplicationFactory<Program>()
                .WithWebHostBuilder(builder => builder.ConfigureServices(services => { }));


            var client = application.CreateClient();

            var result = await client.PostAsJsonAsync("/api/v1/addrating?id=22", new UserRating
            {
                Username = "ventritest@mail.com",
                Rating = 5,
                Comments = "Vintri has enabled us to establish a verified single source of truth for our supply chain."
            });

            Assert.Equal(HttpStatusCode.OK, result.StatusCode);
            Assert.Contains("successfully", await result.Content.ReadAsStringAsync(), StringComparison.InvariantCultureIgnoreCase);
        }



        [Fact]
        public async Task InsertInvalidIdRatingID()
        {
            // Add a valid rating to an invalid beer id and assess that the isertion fails and the related message is returned
            await using var application = new WebApplicationFactory<Program>()
                .WithWebHostBuilder(builder => builder.ConfigureServices(services => { }));

            var client = application.CreateClient();

            var result = await client.PostAsJsonAsync("/api/v1/addrating?id=5564454", new UserRating()
            {
                Username = "ventritest@mail.com",
                Rating = 4,
                Comments = "Vintri has enabled us to establish a verified single source of truth for our supply chain."
            });

            Assert.Contains("Invalid", await result.Content.ReadAsStringAsync());
        }



        [Fact]
        public async Task InsertInvalidRatingBody()
        {
            await using var application = new WebApplicationFactory<Program>()
                .WithWebHostBuilder(builder => builder.ConfigureServices(services => { }));
            var client = application.CreateClient();


            string comments = "This is a simple test comment";
            // Duplicate the comments enough to exceed 1000 characters 
            for (int i = 0; i < 10; i++)
                comments += comments;

            var result = await client.PostAsJsonAsync("/api/v1/addrating?id=5", new UserRating() { Username = "Ventri", Comments = comments });

            Assert.Equal(HttpStatusCode.BadRequest, result.StatusCode);

            var obj = await result.Content.ReadFromJsonAsync<dynamic>();

            Assert.NotNull(obj);
            Assert.True(obj?.ToString().Contains("email address such as contoso13@mymail.com"));
            Assert.True(obj?.ToString().Contains("between 1 and 5."));
            Assert.True(obj?.ToString().Contains("The comments must not exceed 1000 characters."));
        }
    }
}